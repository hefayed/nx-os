#!/usr/bin/env python3
import sys
import csv
import yaml
import yamlloader

users = [{'vlan_id': 100, 'vni': 44100, 'name': 'L2BD100'}]

l2vni_data = []
my_csv_file = '/home/contiv/vxlan-evpn-multi-site/bgw_l2vni_vars.csv'
my_vars_file = '/home/contiv/vxlan-evpn-multi-site/group_vars/bgw.yml'

with open(my_csv_file) as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        l2vni_data.append(dict(row))

with open(my_vars_file) as data:
    data_loaded = yaml.load(data, Loader=yaml.FullLoader)
    # yaml.dump(data_loaded['bgp_neighbors_site_1'][0], sys.stdout)

data_loaded['bgp_neighbors_site_1'][0]['l2vni'] = l2vni_data


with open(my_vars_file, 'w') as my_new_vars:
    my_new_vars.write('---\n')
    data = yaml.dump(data_loaded, my_new_vars)
